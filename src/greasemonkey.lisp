;; Copyright (C) 2019  Jay Kamat <jaygkamat@gmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :jhide)

(defconstant +JS-MAX-RULES+ 3000)
(defparameter *CSS-DISPLAY-NONE* "{display:none !important;}")
(defparameter *CSS-OVERRIDE-DISPLAY-NONE* "{display:unset !important;}")

(defparameter *GM-HEADER*
  "// ==UserScript==
// @name JHide Content Blocking
// @run-at document-end
// @qute-js-world user
// ==/UserScript==

")

(defun chunk-list (chunk-size list)
  "Split LIST into max CHUNK-SIZE sized chunks"
  (let ((newlist nil)
        (part nil)
        (num 0))
    (dolist (elt list)
      (push elt part)
      (incf num)
      (when (>= num chunk-size)
        (push part newlist)
        (setq part nil
              num 0)))
    (push part newlist)
    (nreverse newlist)))

(defun var-to-js (name var)
  (format nil "const ~A = ~A;~%"
          name
          (cl-json:encode-json-to-string var)))

(defun greasemonkey-generate (unconditional fallback hits exceptions whitelist)
  "Generate a greasemonkey script from the arguments passed, a global list of css rules"
  ;; properly removing entries from unconditional would be even more
  ;; expensive, so we hack it by overriding our own rules.

  ;; TODO determine cost properly and concatenate everything in JS
  ;; if that's acceptible.
  (setf unconditional
        (remove-duplicates
         (a:nconcf fallback unconditional)
         :test #'equal)
        fallback nil)

  (str:concat
   *GM-HEADER*

   (var-to-js "exceptions" exceptions)
   (var-to-js "hits" hits)
   (var-to-js "fallback" fallback)
   (var-to-js "whitelist" whitelist)

   (ps:ps
    (defun set-union (a b)
      "Set union of A and B. Destroys set A."
      ((ps:@ b for-each)
       (lambda (x)
         (-> x
             ((ps:@ a add)))))
      a)

    (defun set-difference (a b)
      "Set difference of A and B. Destroys set A."
      ((ps:@ b for-each)
       (lambda (x)
         (-> x
             ((ps:@ a delete)))))
      a)

    (defun css-from-iter (iter &optional undo)
      "Convert this iterable into a css string.
If UNDO, unset the css instead of setting it."
      ;; TODO chunk list into smaller pieces.
      ;; Right now we assume we can get away with one rule here.
      (-> ((ps:@ -Array from) iter)
          (ps:chain (join ","))
          (ps:chain (concat (if undo
                                (ps:lisp *CSS-OVERRIDE-DISPLAY-NONE*)
                                (ps:lisp *CSS-DISPLAY-NONE*))))))

    (defun split-domain ()
      "Split domain into sections. google.com -> (google.com com)"
      (loop
         for domain = (ps:@ window location hostname) then domain
         for domain-index = (ps:chain domain (index-of "."))
         for domain = domain then
           (->> domain-index
                1+
                ((ps:@ domain substring)))
         while (>= domain-index 0)
         collecting domain))
    (setf domain-list (split-domain)
          is-exception
          ((ps:@ domain-list some)
           (lambda (d) (-boolean (ps:getprop whitelist d)))))
    ;; Add unconditional rules
    (unless is-exception
      (-G-M_add-Style
       (ps:lisp (->> unconditional
                     (chunk-list +JS-MAX-RULES+)
                     (mapcar (a:curry #'str:join ","))
                     (mapcar (a:rcurry #'str:concat *CSS-DISPLAY-NONE*))
                     (str:unlines))))

      ;; Handle domain rules
      (let ((exceptions-in-flight (ps:new (-Set)))
            (rules-in-flight (ps:new (-Set)))
            (final-rules (ps:new (-Set fallback))))
        (dolist (domain domain-list)
          (a:when-let ((current (ps:getprop exceptions domain)))
            (set-union
             exceptions-in-flight
             (ps:chain current
                       (filter
                        (lambda (x)
                          (not (ps:chain rules-in-flight (has x))))))))
          (a:when-let ((current (ps:getprop hits domain)))
            (set-union
             rules-in-flight
             (ps:chain current
                       (filter
                        (lambda (x)
                          (not (ps:chain exceptions-in-flight (has x)))))))))
        (set-difference final-rules exceptions-in-flight)
        (set-union final-rules rules-in-flight)
        (when
            (->> (ps:@ final-rules size)
                 (eql 0)
                 (not))
          (-G-M_add-Style
           (css-from-iter final-rules)))
        ;; This isn't normally needed, but we may need to override global
        ;; rules as well.
        (when (->> (ps:@ exceptions-in-flight size)
                   (eql 0)
                   (not))
          (-G-M_add-Style
           (css-from-iter exceptions-in-flight t)))
        nil)))
   (format nil "~%~%")))
