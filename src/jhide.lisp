;; Copyright (C) 2019  Jay Kamat <jaygkamat@gmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :jhide)

;; File reader
(defun read-file-as-lines (filename)
  "Read file into a list of lines.
  Print an error to stdout and quit if file does not exist."
  (with-open-file (in filename :if-does-not-exist nil)
    (if in
        (loop for line = (read-line in nil nil)
           while line
           collect line)
        (progn
          (format *error-output* "fatal: file ~s does not exist!~%" filename)
          (unix-opts:exit 1)))))

(defun write-file (name content)
  (with-open-file
      (stream name
              :direction :output
              :if-exists :supersede
              :if-does-not-exist :create)
    (format stream "~A" content))
  name)

;; CLI arguments and entrypoint
(defun help-quit ()
  (unix-opts:describe
   :prefix "A tool to generate greasemonkey scripts from adblock lists."
   :usage-of "./jhide"
   :args     "[abp style hostlists...]")
  (unix-opts:exit 1))

(defun main (&rest args)
  (declare (ignore args))
  ;; Define arguments
  (unix-opts:define-opts
    (:name :help
           :description "print this help text"
           :short #\h
           :long "help")
    (:name :output
           :description "write result to this file instead of stdout."
           :short #\o
           :arg-parser #'identity
           :long "output")
    (:name :whitelist
           :description "A list of comma-seperated domains which are totally whitelisted.
Will cascade (a.com will whitelist b.a.com)"
           :short #\w
           :arg-parser #'identity
           :long "whitelist"))

  (multiple-value-bind (options free-args)
      (handler-case
          (unix-opts:get-opts)
        (unix-opts:missing-arg (condition)
          (format *error-output* "fatal: option ~s needs an argument!~%"
                  (unix-opts:option condition))
          (unix-opts:exit 1))
        (unix-opts:arg-parser-failed (condition)
          (format *error-output* "fatal: cannot parse ~s as argument of ~s~%"
                  (unix-opts:raw-arg condition)
                  (unix-opts:option condition))
          (unix-opts:exit 1))
        (unix-opts:missing-required-option (con)
          (format *error-output* "fatal: ~a~%" con)
          (unix-opts:exit 1))
        (unix-opts:unknown-option (con)
          (format *error-output* "warning: ~s option is unknown!~%"
                  (opts:option con))
          (unix-opts:exit 1)))

    (when (getf options :help)
      (help-quit))
    (unless free-args
      (format *error-output* "fatal: Adblock files to read are required!~%")
      (unix-opts:exit 1))

    ;; Add all rules to map
    (->> free-args
         (mapcar #'read-file-as-lines)
         (apply #'nconc)
         (mapc #'cssset-add-rule))

    ;; Add whitelisted domains
    (->>
     (getf options :whitelist)
     (str:split ",")
     (mapc #'cssset-add-domain-whitelist))

    (a:if-let
        ((gs-str (cssset-to-greasemonkey))
         (output (getf options :output)))
      (write-file output gs-str)
      (format t "~A" gs-str))

    (unix-opts:exit 0)))
