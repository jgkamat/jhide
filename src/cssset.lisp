;; Copyright (C) 2019  Jay Kamat <jaygkamat@gmail.com>
;;
;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :jhide)

;; Should we replace this with an object so multiple sets can be made?
(defparameter *CSSSET* nil
  "List of unconditional css rules")
(defparameter *CSS-DOMAIN-FALLBACK* nil
  "List of rules that will be taken unless overriden.")
(defparameter *CSS-DOMAIN-HITS*
  (make-hash-table :test #'equal)
  "A map from domain -> css rules")
(defparameter *CSS-DOMAIN-EXCEPTIONS*
  (make-hash-table :test #'equal)
  "A map from domain exemptions -> css rules")
(defparameter *DOMAIN-WHITELIST*
  (make-hash-table :test #'equal)
  "A list of domains that should be a whitelist (disabling all rules).")

(defun cssset-add-domain-rule (options css-rule &key invert)
  "Add a SPLIT-RULE with given domain OPTIONS.
If INVERT, treat exceptions as hits, and vice versa."
  (let* ((domains (str:split "," options))
         (exception-p (->> (a:curry #'str:starts-with-p "~")
                           (a:compose (if invert #'not #'identity)))))
    (when (every exception-p domains)
      ;; If every rule is an exception, add the rule to the general list as well.
      (push css-rule *CSS-DOMAIN-FALLBACK*))
    (dolist (opt domains)
      (let ((exception (funcall exception-p opt))
            (opt (string-left-trim '(#\~) opt)))
        (->> (if exception
                 *CSS-DOMAIN-EXCEPTIONS*
                 *CSS-DOMAIN-HITS*)
             (gethash opt)
             (push css-rule))))))

(defun cssset-add-rule (rule)
  (setf rule (str:trim rule))
  (cond
    ((and (not (str:starts-with-p "!" rule))
          (not (str:starts-with-p "[Adblock" rule))
          (or (str:containsp "##" rule)
              (str:containsp "#@#" rule)))
     (let* ((flip-exceptions (str:containsp "#@#" rule))
            (split-key (if flip-exceptions "#@#" "##"))
            (split-rule (str:split split-key rule))
            (options (first split-rule))
            (css-rule (apply #'str:concat (rest split-rule))))
       (cond
         ((str:emptyp css-rule)
          ;; empty rule
          nil)
         ((not (str:emptyp options))
          ;; Have a conditional rule, handle in helper
          (cssset-add-domain-rule options css-rule :invert flip-exceptions))
         (t
          ;; got a css rule but no options, add to unconditional list
          (push css-rule *CSSSET*)))))
    (t
     ;; Ignore unsupported rules
     nil)))

(defun cssset-add-domain-whitelist (domain)
  "Add DOMAIN to the domain whitelist."
  (setf (gethash domain *DOMAIN-WHITELIST*) t))

(defun cssset-to-greasemonkey ()
  (greasemonkey-generate
   *CSSSET* *CSS-DOMAIN-FALLBACK*
   *CSS-DOMAIN-HITS* *CSS-DOMAIN-EXCEPTIONS*
   *DOMAIN-WHITELIST*))
