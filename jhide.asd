(asdf:defsystem #:jhide
    :description "A tool for generating greasemonkey scripts from adblock lists"
    :author "Jay Kamat <jaygkamat@gmail.com>"
    :license "GPLv3"
    :serial t
    :depends-on (:parenscript
                 :str
                 :unix-opts
                 :alexandria
                 :arrow-macros
                 :cl-json)
    :components ((:module "src"
                          :components
                          ((:file "package")
                           (:file "greasemonkey")
                           (:file "cssset" :depends-on ("greasemonkey"))
                           (:file "jhide" :depends-on ("cssset"))))))
