

# Simple makefile loading the main file, using sbcl. Similar steps can be taken for other lisps
FLAGS=--output run/jhide --asdf-path . --load-system jhide --entry jhide:main
JHIDE_FLAGS=
SBCL_FLAGS=--noinform \
					 --eval "(push (truename \".\") asdf:*central-registry*)" \
					 --eval "(ql:quickload :jhide)"
SBCL_TAIL_FLAGS=--end-toplevel-options $(JHIDE_FLAGS)
NONINTERACTIVE=--non-interactive

RUN_FLAG=--eval "(jhide:main)"
IN_PKG=--eval "(in-package :jhide)"

all:
	sbcl $(SBCL_FLAGS) $(NONINTERACTIVE) $(SBCL_TAIL_FLAGS)

dir:
	mkdir -p run

run:
	sbcl $(SBCL_FLAGS) $(RUN_FLAG) $(SBCL_TAIL_FLAGS)
r: run

debug:
	sbcl $(SBCL_FLAGS) --eval "(declaim (optimize (debug 3)))" $(IN_PKG) $(SBCL_TAIL_FLAGS)

clean:
	-rm -rf ./run
	-find . -name '*.fasl' | xargs rm -f

release:
	sbcl --load build.lisp --name jhide --binary jhide

win:
	wine sbcl --load build.lisp --name jhide --binary jhide.exe

altrel:dir
	buildapp --load ~/.sbclrc --compress-core $(FLAGS)

.PHONY: run
