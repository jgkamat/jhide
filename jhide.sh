#!/bin/sh

# Quick and dirty script to run without any hassle.
# If you need quicker startup, please build an executable instead.

sbcl --noinform --eval "(push (truename \"$(dirname "$0")\") asdf:*central-registry*)" \
	 --eval "(ql:quickload :jhide :silent t)" --eval "(jhide:main)" --end-toplevel-options "$@"
