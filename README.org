
* jhide - j's hider

~jhide~ is a simple tool for generating greasemonkey scripts from abp style
block lists.

* Features
We currently only support basic css hiding rules.

Any ~#?#~ rules or network blocking rules are ignored.

* Installation

Install [[https://www.quicklisp.org/beta/#installation][quicklisp]] and sbcl and add it to your init file, so ql is available.

Use the [[file:jhide.sh][jhide.sh]] script for a quickstart. If you want more options (such as
building a binary release), please see the makefile.

Run with ~-h~ to get help on more options.

By default, the generated script will be outputted to stdout, but this can be
changed with ~-o~.

* Example usage

#+BEGIN_SRC sh
  # help
  jhide --help

  # simple run
  jhide my-rules

  # qutebrowser with jblock
  jhide ~/.local/share/qutebrowser/jblock-rules -o ~/.local/share/qutebrowser/greasemonkey/jhide.js
#+END_SRC

* Future Improvements
- Right now, we load all global styles on page load, which can add significant
  overhead (~10ms in my testing). Doing this differently may be more efficient,
  but would make it harder to measure the overhead.
- In order to avoid overhead and complexity in js, override rules are not
  removed completely, but instead overridden with CSS. This may cause site
  styling of the ~display~ property to be overriden to ~unset~ when rules are
  overridden.
- We currently load on ~document-end~ which leads to a short period of seeing
  the ad before it's blocked. Moving to ~document-start~ will fix this, but
  that has the possibility of being too early and not running. Fixing this
  with timers may cause the script to stop working when js is disabled due to
  [[http://bugreports.qt.io/browse/QTBUG-74304][this qtbug]].

* License
jhide is licensed under the GPLv3+.
